/*  DShot - High Performance WS2811 LED Display Library
    http://www.pjrc.com/teensy/td_libs_DShot.html
    Copyright (c) 2013 Paul Stoffregen, PJRC.COM, LLC
    Some Teensy-LC support contributed by Mark Baysinger.
    https://forum.pjrc.com/threads/40863-Teensy-LC-port-of-DShot
    Modified to support output only on selected channels by Aleksi Turunen

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <string.h>
#include "DShot.h"

void * DShot::frameBuffer;
void * DShot::drawBuffer;
uint8_t DShot::params;
DMAChannel DShot::dma1;
DMAChannel DShot::dma2;
DMAChannel DShot::dma3;

static uint8_t ones = 0xFF;
static volatile uint8_t update_in_progress = 0;
static uint32_t update_completed_at = 0;

DShot::DShot(void *frameBuf, void *drawBuf, uint8_t config)
{
  frameBuffer = frameBuf;
  drawBuffer = drawBuf;
  params = config;
  outputMask = 0xFFFFFFFF;
  outMask = 0xFF;
}

// Waveform timing: these set the high time for a 0 and 1 bit, as a fraction of
// the total 800 kHz or 400 kHz clock cycle.  The scale is 0 to 255.  The Worldsemi
// datasheet seems T1H should be 600 ns of a 1250 ns cycle, or 48%.  That may
// erroneous information?  Other sources reason the chip actually samples the
// line close to the center of each bit time, so T1H should be 80% if TOH is 20%.
// The chips appear to work based on a simple one-shot delay triggered by the
// rising edge.  At least 1 chip tested retransmits 0 as a 330 ns pulse (26%) and
// a 1 as a 660 ns pulse (53%).  Perhaps it's actually sampling near 500 ns?
// There doesn't seem to be any advantage to making T1H less, as long as there
// is sufficient low time before the end of the cycle, so the next rising edge
// can be detected.  T0H has been lengthened slightly, because the pulse can
// narrow if the DMA controller has extra latency during bus arbitration.  If you
// have an insight about tuning these parameters AND you have actually tested on
// real LED strips, please contact paul@pjrc.com.  Please do not email based only
// on reading the datasheets and purely theoretical analysis.

#define WS2811_TIMING_T0H  105  //95 - 105 calculated values vs tuned values
#define WS2811_TIMING_T1H  200  //191 - 200

// Discussion about timing and flicker & color shift issues:
// http://forum.pjrc.com/threads/23877-WS2812B-compatible-with-DShot-library?p=38190&viewfull=1#post38190

void DShot::begin(void *frameBuf, void *drawBuf, uint8_t config)
{
  frameBuffer = frameBuf;
  drawBuffer = drawBuf;
  params = config;
  begin();
}

void DShot::begin(void)
{
  uint32_t bufsize, frequency;
  bufsize = 16;

  // set up the buffers
  memset(frameBuffer, 0xFF, bufsize);  //Buffer logic is inverted due to using pin clear register to write data, logic 1 means "low" bit so off is full ones
  if (drawBuffer) {
    memset(drawBuffer, 0xFF, bufsize);
  } else {
    drawBuffer = frameBuffer;
  }

  // output pins configured as outputs in the "enableChannel" method

  // create the two waveforms for DShot low and high bits
  switch (params & 0xF0) {
    case DSHOT_150:
      frequency = 150000;
      break;
    case DSHOT_300:
      frequency = 300000;
      break;
    case DSHOT_600:
      frequency = 600000;
      break;
    default:
      frequency = 600000;
  }



#if defined(__MK20DX128__)
  FTM1_SC = 0;
  FTM1_CNT = 0;
  uint32_t mod = (F_BUS + frequency / 2) / frequency;
  FTM1_MOD = mod - 1;
  FTM1_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM1_C0SC = 0x69;
  FTM1_C1SC = 0x69;
  FTM1_C0V = (mod * WS2811_TIMING_T0H) >> 8;
  FTM1_C1V = (mod * WS2811_TIMING_T1H) >> 8;
  // pin 16 triggers DMA(port B) on rising edge
  CORE_PIN16_CONFIG = PORT_PCR_IRQC(1) | PORT_PCR_MUX(3);
  //CORE_PIN4_CONFIG = PORT_PCR_MUX(3); // testing only

#elif defined(__MK20DX256__)
  FTM2_SC = 0;
  FTM2_CNT = 0;
  uint32_t mod = (F_BUS + frequency / 2) / frequency;
  FTM2_MOD = mod - 1;
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x69;
  FTM2_C0V = (mod * WS2811_TIMING_T0H) >> 8;
  FTM2_C1V = (mod * WS2811_TIMING_T1H) >> 8;
  // pin 32 is FTM2_CH0, PTB18, triggers DMA(port B) on rising edge
  // pin 25 is FTM2_CH1, PTB19
  CORE_PIN32_CONFIG = PORT_PCR_IRQC(1) | PORT_PCR_MUX(3);
  //CORE_PIN25_CONFIG = PORT_PCR_MUX(3); // testing only

#elif defined(__MK64FX512__) || defined(__MK66FX1M0__)
  FTM2_SC = 0;
  FTM2_CNT = 0;
  uint32_t mod = (F_BUS + frequency / 2) / frequency;
  FTM2_MOD = mod - 1;
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x69;
  FTM2_C0V = (mod * WS2811_TIMING_T0H) >> 8;
  FTM2_C1V = (mod * WS2811_TIMING_T1H) >> 8;
  // FTM2_CH0, PTA10 (not connected), triggers DMA(port A) on rising edge
  PORTA_PCR10 = PORT_PCR_IRQC(1) | PORT_PCR_MUX(3);

#elif defined(__MKL26Z64__)
  FTM2_SC = 0;
  FTM2_CNT = 0;
  uint32_t mod = F_CPU / f  requency;
  FTM2_MOD = mod - 1;
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0);
  FTM2_C0SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB;
  FTM2_C1SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB;
  TPM2_C0V = mod - ((mod * WS2811_TIMING_T1H) >> 8);
  TPM2_C1V = mod - ((mod * WS2811_TIMING_T1H) >> 8) + ((mod * WS2811_TIMING_T0H) >> 8);

#endif

  // DMA channel #1 sets WS2811 high at the beginning of each cycle
  dma1.source(outMask);
  dma1.destination(GPIOC_PSOR);
  dma1.transferSize(1);
  dma1.transferCount(bufsize);
  dma1.disableOnCompletion();

  // DMA channel #2 writes the pixel data at 23% of the cycle
  dma2.sourceBuffer((uint8_t *)frameBuffer, bufsize);
  dma2.destination(GPIOC_PCOR);
  dma2.transferSize(1);
  dma2.transferCount(bufsize);
  dma2.disableOnCompletion();

  // DMA channel #3 clear all the pins low at 69% of the cycle
  dma3.source(outMask);
  dma3.destination(GPIOC_PCOR);
  dma3.transferSize(1);
  dma3.transferCount(bufsize);
  dma3.disableOnCompletion();
  dma3.interruptAtCompletion();

#if defined(__MK20DX128__)
  // route the edge detect interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_PORTB);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM1_CH0);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM1_CH1);
  DMAPriorityOrder(dma3, dma2, dma1);
#elif defined(__MK20DX256__)
  // route the edge detect interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_PORTB);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH0);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH1);
  DMAPriorityOrder(dma3, dma2, dma1);
#elif defined(__MK64FX512__) || defined(__MK66FX1M0__)
  // route the edge detect interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_PORTA);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH0);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_CH1);
  DMAPriorityOrder(dma3, dma2, dma1);
#elif defined(__MKL26Z64__)
  // route the timer interrupts to trigger the 3 channels
  dma1.triggerAtHardwareEvent(DMAMUX_SOURCE_TPM2_CH0);
  dma2.triggerAtHardwareEvent(DMAMUX_SOURCE_TPM2_CH1);
  dma3.triggerAtHardwareEvent(DMAMUX_SOURCE_FTM2_OV);
#endif

  // enable a done interrupts when channel #3 completes
  dma3.attachInterrupt(isr);
  //pinMode(9, OUTPUT); // testing: oscilloscope trigger
}

void DShot::isr(void)
{
  //digitalWriteFast(9, HIGH);
  //Serial1.print(".");
  //Serial1.println(dma3.CFG->DCR, HEX);
  //Serial1.print(dma3.CFG->DSR_BCR > 24, HEX);
  dma3.clearInterrupt();
#if defined(__MKL26Z64__)
  GPIOD_PCOR = 0xFF;
#endif
  //Serial1.print("*");
  update_completed_at = micros();
  update_in_progress = 0;
  //digitalWriteFast(9, LOW);
}

int DShot::busy(void)
{
  if (update_in_progress) return 1;
  // busy for 50 (or 300 for ws2813) us after the done interrupt, for WS2811 reset
  if (micros() - update_completed_at < 300) return 1;
  return 0;
}

void DShot::show(void)
{
  // wait for any prior DMA operation
  //Serial1.print("1");
  //while (update_in_progress) ;  //Use this to ensue everything always gets shown
  if (update_in_progress) {       //Use this to prevent delays here, even for a while, good for flight controllers or other time critical stuff
    return;   //Don't do anything if currently busy
  }
  //Serial1.print("2");
  // it's ok to copy the drawing buffer to the frame buffer
  // during the 50us WS2811 reset time
  if (drawBuffer != frameBuffer) {
    // TODO: this could be faster with DMA, especially if the
    // buffers are 32 bit aligned... but does it matter?
    memcpy(frameBuffer, drawBuffer, 16);
  }

  // wait for WS2811 reset
  // while (micros() - update_completed_at < 300) ;
  // ok to start, but we must be very careful to begin
  // without any prior 3 x 800kHz DMA requests pending

  //If not ok to start, do nothing to prevent delays
  if (micros() - update_completed_at < 300) {
    return;
  }

#if defined(__MK20DX128__)
  uint32_t cv = FTM1_C0V;
  noInterrupts();
  // CAUTION: this code is timing critical.
  while (FTM1_CNT <= cv) ;
  while (FTM1_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM1_CNT < cv) ;
  FTM1_SC = 0;            // stop FTM1 timer (hopefully before it rolls over)
  FTM1_CNT = 0;
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
  PORTB_ISFR = (1 << 0);  // clear any prior rising edge
  uint32_t tmp __attribute__((unused));
  FTM1_C0SC = 0x28;
  tmp = FTM1_C0SC;        // clear any prior timer DMA triggers
  FTM1_C0SC = 0x69;
  FTM1_C1SC = 0x28;
  tmp = FTM1_C1SC;
  FTM1_C1SC = 0x69;
  dma1.enable();
  dma2.enable();          // enable all 3 DMA channels
  dma3.enable();
  FTM1_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0); // restart FTM1 timer
  //digitalWriteFast(9, LOW);

#elif defined(__MK20DX256__)
  FTM2_C0SC = 0x28;
  FTM2_C1SC = 0x28;
  uint32_t cv = FTM2_C0V;
  noInterrupts();
  // CAUTION: this code is timing critical.
  while (FTM2_CNT <= cv) ;
  while (FTM2_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM2_CNT < cv) ;
  FTM2_SC = 0;             // stop FTM2 timer (hopefully before it rolls over)
  FTM2_CNT = 0;
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
  PORTB_ISFR = (1 << 18);  // clear any prior rising edge
  uint32_t tmp __attribute__((unused));
  FTM2_C0SC = 0x28;
  tmp = FTM2_C0SC;         // clear any prior timer DMA triggers
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x28;
  tmp = FTM2_C1SC;
  FTM2_C1SC = 0x69;
  dma1.enable();
  dma2.enable();           // enable all 3 DMA channels
  dma3.enable();
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0); // restart FTM2 timer
  //digitalWriteFast(9, LOW);

#elif defined(__MK64FX512__) || defined(__MK66FX1M0__)
  FTM2_C0SC = 0x28;
  FTM2_C1SC = 0x28;
  uint32_t cv = FTM2_C1V;
  noInterrupts();
  // CAUTION: this code is timing critical.
  while (FTM2_CNT <= cv) ;
  while (FTM2_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM2_CNT < cv) ;
  FTM2_SC = 0;             // stop FTM2 timer (hopefully before it rolls over)
  FTM2_CNT = 0;
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
#if defined(__MK64FX512__)
  asm("nop");
#endif
  PORTA_ISFR = (1 << 10);  // clear any prior rising edge
  uint32_t tmp __attribute__((unused));
  FTM2_C0SC = 0x28;
  tmp = FTM2_C0SC;         // clear any prior timer DMA triggers
  FTM2_C0SC = 0x69;
  FTM2_C1SC = 0x28;
  tmp = FTM2_C1SC;
  FTM2_C1SC = 0x69;
  dma1.enable();
  dma2.enable();           // enable all 3 DMA channels
  dma3.enable();
  FTM2_SC = FTM_SC_CLKS(1) | FTM_SC_PS(0); // restart FTM2 timer
  //digitalWriteFast(9, LOW);

#elif defined(__MKL26Z64__)
  uint32_t sc __attribute__((unused)) = FTM2_SC;
  uint32_t cv = FTM2_C1V;
  noInterrupts();
  while (FTM2_CNT <= cv) ;
  while (FTM2_CNT > cv) ; // wait for beginning of an 800 kHz cycle
  while (FTM2_CNT < cv) ;
  FTM2_SC = 0;		// stop FTM2 timer (hopefully before it rolls over)
  update_in_progress = 1;
  //digitalWriteFast(9, HIGH); // oscilloscope trigger
  dma1.clearComplete();
  dma2.clearComplete();
  dma3.clearComplete();
  uint32_t bufsize = 16;
  dma1.transferCount(bufsize);
  dma2.transferCount(bufsize);
  dma3.transferCount(bufsize);
  dma2.sourceBuffer((uint8_t *)frameBuffer, bufsize);
  // clear any pending event flags
  FTM2_SC = FTM_SC_TOF;
  FTM2_C0SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB | FTM_CSC_DMA;
  FTM2_C1SC = FTM_CSC_CHF | FTM_CSC_MSB | FTM_CSC_ELSB | FTM_CSC_DMA;
  // clear any prior pending DMA requests
  dma1.enable();
  dma2.enable();		// enable all 3 DMA channels
  dma3.enable();
  FTM2_CNT = 0; // writing any value resets counter
  FTM2_SC = FTM_SC_DMA | FTM_SC_CLKS(1) | FTM_SC_PS(0);
  //digitalWriteFast(9, LOW);
#endif
  //Serial1.print("3");
  interrupts();
  //Serial1.print("4");
}

void DShot::setMotor(uint32_t motor, bool telemRequest, uint32_t throttle) {
  throttle = max(0, min(1999, throttle)); //Make sure this is in the correct range, 0-1999
  Set(motor, telemRequest, throttle);
}
void DShot::setCommand(uint32_t motor, uint32_t cmd) {
  if (cmd >= 1 && cmd <= 47) { //Only send a command if it's in the right range
    Set(motor, true, cmd);
  }
}

void DShot::Set(uint32_t motor, bool telemRequest, uint32_t value)
{
  uint32_t mask32, *p;
  uint32_t packet = 0; //This is the data packet to be sent

  if (value && value < 48) { //This is a command value in range 1-47
    packet = value << 1; //Move it over to it's right place
  }
  else { //This is a throttle value, either 0 or 48-2047
    if (value) packet = (value + 48) << 1; //Move throttle value to make way for telemetry bit, add offset of 48 if not zero and move it over to the right place
  }

  if (telemRequest) packet |= 0x01; //Add telemetry bit

  uint32_t checksum = 0;
  uint32_t checksumData = packet;

  for (int i = 0; i < 3; i++) {
    checksum ^= checksumData; // xor data by nibbles
    checksumData >>= 4;
  }
  checksum &= 0xf;

  packet = (packet << 4) | checksum;  //Throttle and commands have 11 first bits in the 16 bit frame, so scoot them 5 bits over to their place.

  p = ((uint32_t *) drawBuffer);

  mask32 = (0x01010101) << motor; //for blanking all bit corresponding to one strip

  //Flip the low/high logic levels since we're using pin clear register instead of data out register (logic 1 clears the port, logic 0 does nothing)

  // Set bytes 0-3
  *p |= mask32;
  *p &= ~((((packet & 0x8000) >> 15) | ((packet & 0x4000) >> 6) | ((packet & 0x2000) << 3) | ((packet & 0x1000) << 12)) << motor);
  *p &= outputMask;

  // Set bytes 4-7
  *++p |= mask32;
  *p &= ~((((packet & 0x800) >> 11) | ((packet & 0x400) >> 2) | ((packet & 0x200) << 7) | ((packet & 0x100) << 16)) << motor);
  *p &= outputMask;

  // Set bytes 8-11
  *++p |= mask32;
  *p &= ~((((packet & 0x80) >> 7) | ((packet & 0x40) << 2) | ((packet & 0x20) << 11) | ((packet & 0x10) << 20)) << motor);
  *p &= outputMask;

  // Set bytes 12-15
  *++p |= mask32;
  *p &= ~((((packet & 0x8) >> 3) | ((packet & 0x4) << 6) | ((packet & 0x2) << 15) | ((packet & 0x1) << 24)) << motor);
  *p &= outputMask;
}

void DShot::enableChannel(uint8_t num) {
  if (num < 8) {       //if channel is in range, add correct output masking bits
    while (update_in_progress) ; //Don't modify these if leds are being updates.
    outMask |= (1 << num);
    outputMask |= (0x01010101) << num;
    //Set corresponding pin as output
    switch (num) {
      case 0:
        pinMode(15, OUTPUT);  // 
        break;
      case 1:
        pinMode(22, OUTPUT);  // Temeriaves servo 7
        break;
      case 2:
        pinMode(23, OUTPUT);  // Temeriaves servo 8
        break;
      case 3:
        pinMode(9, OUTPUT);   // 
        break;
      case 4:
        pinMode(10, OUTPUT);  // 
        break;
      case 5:
        pinMode(13, OUTPUT);  // Temeriaves DRV3
        break;
      case 6:
        pinMode(11, OUTPUT);  // 
        break;
      case 7:
        pinMode(12, OUTPUT);  // 
        break;
      default:
        break;
    }
  }
}
