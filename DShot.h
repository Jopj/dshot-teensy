/*  DShot - High Performance WS2811 LED Display Library
    http://www.pjrc.com/teensy/td_libs_DShot.html
    Copyright (c) 2013 Paul Stoffregen, PJRC.COM, LLC

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#ifndef DShot_h
#define DShot_h

#ifdef __AVR__
#error "Sorry, DShot only works on 32 bit Teensy boards.  AVR isn't supported."
#endif

#include <Arduino.h>
#include "DMAChannel.h"

#if TEENSYDUINO < 121
#error "Teensyduino version 1.21 or later is required to compile this library."
#endif
#ifdef __AVR__
#error "DShot does not work with Teensy 2.0 or Teensy++ 2.0."
#endif

#define WS2811_RGB	0	// The WS2811 datasheet documents this way
#define WS2811_RBG	1
#define WS2811_GRB	2	// Most LED strips are wired this way
#define WS2811_GBR	3
#define WS2811_BRG	4
#define WS2811_BGR	5

#define DSHOT_150 0x00	// 
#define DSHOT_300 0x10	// 
#define DSHOT_600 0x20	// 

  enum dshot_cmd {
  DSHOT_CMD_MOTOR_STOP = 0,
  DSHOT_CMD_BEACON1,
  DSHOT_CMD_BEACON2,
  DSHOT_CMD_BEACON3,
  DSHOT_CMD_BEACON4,
  DSHOT_CMD_BEACON5,
  DSHOT_CMD_ESC_INFO,
  DSHOT_CMD_SPIN_DIRECTION_1,
  DSHOT_CMD_SPIN_DIRECTION_2,
  DSHOT_CMD_3D_MODE_OFF,
  DSHOT_CMD_3D_MODE_ON,
  DSHOT_CMD_SETTINGS_REQUEST,
  DSHOT_CMD_SAVE_SETTINGS,
  DSHOT_CMD_SPIN_DIRECTION_NORMAL = 20,
  DSHOT_CMD_SPIN_DIRECTION_REVERSED = 21,
  DSHOT_CMD_LED0_ON,  //Blue
  DSHOT_CMD_LED1_ON,  //Red
  DSHOT_CMD_LED2_ON,  //Green
  DSHOT_CMD_LED3_ON,  //None
  DSHOT_CMD_LED0_OFF,
  DSHOT_CMD_LED1_OFF,
  DSHOT_CMD_LED2_OFF,
  DSHOT_CMD_LED3_OFF,
  DSHOT_CMD_AUDIO_STREAM_MODE_ON_OFF = 30,
  DSHOT_CMD_SILENT_MODE_ON_OFF = 31,
  DSHOT_CMD_MAX = 47
};


class DShot {
public:
	DShot(void *frameBuf, void *drawBuf, uint8_t config = WS2811_GRB);
	void begin(void);
	void begin(void *frameBuf, void *drawBuf, uint8_t config = WS2811_GRB);

	void setMotor(uint32_t num, bool telemRequest, uint32_t throttle);  //Takes throttle value in range 0-2000 and adds that to the DShot frame, as well as telemetry request
  void setCommand(uint32_t num, uint32_t cmd);             //Adds the telemetry request bit to the DShot frame

  void Set(uint32_t num, bool telemRequest, uint32_t throttle);  //Actually modifies the buffer to be sent, used by setMotor and setCommand.

  void enableChannel(uint8_t num);  //Enables a channel from 0-7. By default none are enabled so this must be used to get any output.

         // pin 2 -> channel 0 strip #1
        // pin 14 -> channel 1 strip #2
       // pin 7 -> channel 2 strip #3
      // pin 8 -> channel 3 strip #4
     // pin 6 -> channel 4 strip #5
    // pin 20 -> channel 5 strip #6
   // pin 21 -> channel 6 strip #7
  // pin 5 -> channel 7 strip #8

	void show(void);
	int busy(void);

	int color(uint8_t red, uint8_t green, uint8_t blue) {
		return (red << 16) | (green << 8) | blue;
	}


private:
	static void *frameBuffer;
	static void *drawBuffer;
	static uint8_t params;
	static DMAChannel dma1, dma2, dma3;
	static void isr(void);
  uint8_t outMask;  //This is used as a source for pulse start and end DMA's replacing fixed "ones"
  uint32_t outputMask;  //This is used to mask the buffer 4 bytes at a time, it is 4 copies of outMask pre-calculated for tiny performance boost
  dshot_cmd _dshot_cmd;

  
};

#endif
