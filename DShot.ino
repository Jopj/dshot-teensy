/*

  Required Connections
  --------------------
    pin 2:  LED Strip #1    OctoWS2811 drives 8 LED Strips.
    pin 14: LED strip #2    All 8 are the same length.
    pin 7:  LED strip #3
    pin 8:  LED strip #4    A 100 ohm resistor should used
    pin 6:  LED strip #5    between each Teensy pin and the
    pin 20: LED strip #6    wire to the LED strip, to minimize
    pin 21: LED strip #7    high frequency ringining & noise.
    pin 5:  LED strip #8
    pin 15 & 16 - Connect together, but do not use
    pin 4 - Do not use
    pin 3 - Do not use as PWM.  Normal use is ok.

  This test is useful for checking if your LED strips work, and which
  color config (WS2811_RGB, WS2811_GRB, etc) they require.
*/

#include "DShot.h"

DMAMEM unsigned int displayMemory[4];
unsigned int drawingMemory[4];

const int config = WS2811_GRB | DSHOT_600;

DShot motors(displayMemory, drawingMemory, config);

void setup() {
  motors.enableChannel(0);
  motors.enableChannel(4);
  motors.enableChannel(2);
  motors.enableChannel(3);
  motors.begin();
  motors.show();
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
}

#define RED    0x110000
#define GREEN  0x001100
#define BLUE   0x000016
#define YELLOW 0x101400
#define PINK   0x120009
#define ORANGE 0x100400
#define WHITE  0x101010


void loop() {
  static float throttle = 0;
  static int counter = 0;
  int microsec = 30000;  // change them all in 2 seconds
  int toPrint = 0;

  // uncomment for voltage controlled speed
  float input = (float)map(analogRead(A0), 0, 1023, -100, 2100);
  throttle = input * 0.001 + throttle * 0.999;
  int toMotor = min(max((int)throttle, 0), 1999);
  if (millis() < 5000) {
    motors.setCommand(0, DSHOT_CMD_LED1_OFF);
  }
  else {
    if (!toMotor) {
      motors.setMotor(0, false, 0);
      toPrint = 0;
    }

    else if (toMotor < 1000) {
      motors.setCommand(0, DSHOT_CMD_LED2_OFF);
      toPrint = 1;
    }

    else {
      motors.setCommand(0, DSHOT_CMD_LED2_ON);
      toPrint = 9;
    }
  }
  motors.setMotor(4, false, 1000);
  motors.setMotor(2, false, 678);
  motors.setMotor(3, false, 1999);
  motors.show();
  counter++;

  if (counter > 1000) {
    counter = 0;
    Serial.println(toPrint);
  }
  delayMicroseconds(100);
}
